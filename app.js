document.addEventListener('DOMContentLoaded', function() {

    let singUpForm = document.getElementById("sing-up-form");
    let postForm = document.getElementById("post-form");
    let logInForm = document.getElementById("login-form");
    let logOutBtn = document.getElementById("log-out")
    let singUpBtn = document.getElementById("singup-btn")
    let loginBtn = document.getElementById("login-btn")
    let guides = document.querySelector(".guides")
    let allLoginBtn = document.querySelectorAll(".login-btn")
    let postContainer = document.querySelector(".post-container")
    let userInfo = document.querySelector(".user-info")
    let loginMessage = document.querySelector(".login-message")
    let toggle = document.querySelector(".toggle-color")
    let result = document.getElementById("result");
    loginMessage.innerHTML = '<h5 class="messages">sign up or login to view contents</h5>'
    // Your web app's Firebase configuration
    var firebaseConfig = {
        apiKey: "AIzaSyC-9s2SVfXGpcH7EBgNOhAbUzqq5E9tLh4",
        authDomain: "authentication-bb85a.firebaseapp.com",
        databaseURL: "https://authentication-bb85a.firebaseio.com",
        projectId: "authentication-bb85a",
        storageBucket: "authentication-bb85a.appspot.com",
        messagingSenderId: "797857956047",
        appId: "1:797857956047:web:ed53d1eae4dedd3e6337ba"
      };
      // Initialize Firebase
      firebase.initializeApp(firebaseConfig);
    
      /// auth ///
    
      const auth = firebase.auth();
    
      /// db ///
    
      const db = firebase.firestore();
    
      db.settings({ timestampsInSnapshots: true });
    
      singUpForm.addEventListener("submit", (e) => {
        e.preventDefault();
        const email = singUpForm['sing-up-email'].value
        const password = singUpForm['sing-up-password'].value
    
        console.log(email,password);
    
        auth.createUserWithEmailAndPassword(email,password).then(data=> {
            console.log(data.user);
            const modal = document.querySelector('#sing-up-modal');
            singUpForm.reset();
        })
      });
      logOutBtn.addEventListener("click", (e) => {
        e.preventDefault();
        auth.signOut().then(() => {
            console.log("user logout");
        })
       });
    
       logInForm.addEventListener("submit", (e) => {
        e.preventDefault();
        const email = logInForm['login-email'].value
        const password = logInForm['login-password'].value
    
        console.log(email,password);
    
        auth.signInWithEmailAndPassword(email,password).then(data=> {
            // if (data.user) {
            //     const html = `
            //     <h1>${data.user.email}</h1>
            //     `
            //       userInfo.innerHTML = html
            //   }else {
            //       userInfo.innerHTML = ""
            //   }
            console.log(data.user.email);
            logInForm.reset();
        })
      });
      const setupUI = (user) => {
        if (user) {
          const html = `
          <span>${user.email}</span>
          `
            userInfo.innerHTML = html
        }else {
            userInfo.innerHTML = ""
        }
        }
      auth.onAuthStateChanged(user=> {
          if (user) {
            // db collection //
           db.collection("guides").onSnapshot((snapshot)=> {
            let changes = snapshot.docChanges();
            console.log(changes);
        
            changes.forEach((change) => {
              if (change.type == "added") {
                firebaseDataHendler(change.doc);
              } else if (change.type == "removed") {
                let article = guides.querySelector("[data-id=" + change.doc.id + "]");
                console.log(change.doc.id);
                guides.removeChild(article);
              }
            });
            //  setupGuides(snapshot.docs)
             setupUI(user)
             allLoginBtn.forEach(item => {
                item.style.display = "none"
            });
            logOutBtn.style.display = "block"
            singUpForm.style.display = "none"
            logInForm.style.display = "none"
            postContainer.style.display = "block"
            loginMessage.innerHTML = ""
           });
          } else {
            allLoginBtn.forEach(item => {
                item.style.display = "block"
            });
            logOutBtn.style.display = "none"
            postContainer.style.display = "none"
            loginMessage.innerHTML = '<h5 class="messages">sign up or login to view contents</h5>'
            singUpBtn.addEventListener("click",function (){
                singUpForm.style.display = "block"
                logInForm.style.display = "none"
            })
            loginBtn.addEventListener("click",function (){
                logInForm.style.display = "block"
                singUpForm.style.display = "none"
            })
            setupUI()
            // setupGuides([])
          }
      });
    // const setupGuides = (data) => {
    //     if (data.length) {
    //         loginMessage.innerHTML = ''
    //     //     let html =''
    //     //     data.forEach(doc => {
    //     //         const guide = doc.data();
    //     //         const element =`
    //     //         <div class="element-div w-75 my-4 rounded">
    //     //         <h2>${guide.title}</h2>
    //     //         <p>${guide.content}</p>
    //     //         <span>${guide.time}</span>
    //     //         <span>${guide.date}</span>
    //     //         </div>
    //     //         `
    //     //        html += element
    //     //     });
    //     //  guides.innerHTML = html
    
    //     }else{
            
    //     }
    
    // } 
    postForm.addEventListener("submit", (e) => {
        e.preventDefault();
        db.collection("guides").add({
            title:postForm['post-title'].value,
            content:postForm['post-content'].value,
            date:new Date().toLocaleDateString(),
            time:new Date().toLocaleTimeString()
        })
       postForm['post-title'].value = ""
       postForm['post-content'].value = ""
    });
    
    function firebaseDataHendler(myData) {
        const article = document.createElement("article");
        const title = document.createElement("h2");
        const content = document.createElement("p");
        const year = document.createElement("span");
        const time = document.createElement("span");
        const button = document.createElement("button");
        button.setAttribute("class", "btn-sm btn-danger my-3");
        button.textContent = "delete"
        article.setAttribute("data-id", myData.id);
        article.setAttribute("class", "element-div w-75 my-4 rounded animate__animated animate__bounceIn");
        title.textContent = myData.data().title;
        content.textContent = myData.data().content;
        year.textContent = myData.data().date;
        time.textContent = myData.data().time;
        article.appendChild(title);
        article.appendChild(content);
        article.appendChild(year);
        article.appendChild(time);
        article.appendChild(button);
      
        guides.appendChild(article);
      
        button.addEventListener("click",function (e) {
          e.stopPropagation()
          let id = e.target.parentElement.getAttribute('data-id');
          db.collection("guides").doc(id).delete()
        })
      }
      toggle.addEventListener("click",toggleDarkMode);
    
      function toggleDarkMode(params) {
          const body = document.querySelector("body");
          body.classList.toggle("dark")
      }
    });